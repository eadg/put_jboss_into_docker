import os
import os.path
import sys
import glob
import socket
import shutil
import tarfile
import unittest
import re
import xml.etree.ElementTree


class Hostname(object):

    def __init__(self):
        self.hostname = self.get_hostname()

    def get_hostname(self):
        ghn = socket.gethostname()
        if not ('0-' or '1-') in ghn:
            return self.remove_suffix(ghn)
        else:
            return self.get_etc_hosts(ghn)

    def get_etc_hosts(self, host):
        try:
            hosts_tuple = socket.gethostbyaddr(host)
            if not hosts_tuple[1]:
                return self.remove_suffix(hosts_tuple[0])
            else:
                listh = [h for h in hosts_tuple[1] if not ('0-' or '1-') in h]
                if any in listh:
                    return self.remove_suffix(listh[0])
                else:
                    return self.remove_suffix(host)
        except (socket.error, socket.herror):
            return self.remove_suffix(host)

    def remove_suffix(self, hostname):
        if 'ultimo.pl' in hostname:
            return hostname.split('.', 1).remove()
        else:
            return hostname


class Package(object):

    def __init__(self):
        self.local = Hostname().hostname
        self.subdir = ['bin', 'deployments']
        self.path = sys.argv[1]

    def mdir(self):
        '''mkdir for folders without subdirectory
            @cpdir() copytree makes directories by self'''
        try:
            os.mkdir(self.local)
            for s in self.subdir:
                os.mkdir(self.local+'/'+s)
        except OSError:
            print('path exists')

    def rdir(self):
        '''remove folders made by previous run script'''
        try:
            shutil.rmtree(self.local, ignore_errors=True)
        except OSError:
            print('path not exists')

    def cpdir(self):
        shutil.copy(self.path + '/profile.conf', self.local)
        shutil.copy(self.path + '/bin/standalone.conf', self.local + '/bin')
        for f in glob.glob(self.path + '/standalone/deployments/*.*ar'):
            shutil.copy(f, self.local + '/deployments')
        shutil.copytree(
            self.path + '/standalone/configuration/', self.local + '/configuration')
        shutil.copytree(self.path + '/modules/', self.local + '/modules')

    def makeTar(self):
        mytar = tarfile.open(self.local + ".tgz", "w:gz")
        mytar.add(self.local)

    def fix_standalone_conf(self):
        fpath = self.local + '/bin/standalone.conf'
        pattern = re.compile('(JAVA_HOME=)')
        with open(fpath) as f:
            data = f.read()
        fix_data = re.sub(pattern, r'#\1', data)
        with open(fpath, 'w+') as f:
            f.write(fix_data)
        return len(fix_data)-len(data)

    def fix_standalone_xml(self):
        fpath = self.local + '/configuration/standalone.xml'
        fx = xml.etree.ElementTree.parse(fpath)
        root = fx.getroot()
        root[4][0][0].set('value', '0.0.0.0')
        root[4][1][0].set('value', '0.0.0.0')
        root[4][2][0].set('value', '0.0.0.0')
        return (root[4][0][0].attrib, root[4][1][0].attrib, root[4][2][0].attrib)

    def do_all(self):
        self.rdir()
        self.mdir()
        self.cpdir()
        self.fix_standalone_conf()
        # self.makeTar()


class TestPackage(unittest.TestCase):

    def test_get_hostname(self):
        self.assertIsInstance(Hostname().get_hostname(), str)

    def test_etc_hosts(self):
        h = Hostname().get_hostname()
        self.assertIsInstance(Hostname().get_etc_hosts(h), str)

    def test_file_exists(self):
        jbhome = sys.argv[1]
        self.assertTrue(os.path.isfile(jbhome + '/profile.conf'))

    def test_fix_standalone_conf(self):
        p = Package()
        p.rdir()
        p.mdir()
        p.cpdir()
        r = p.fix_standalone_conf()
        self.assertEqual(r, 1)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPackage)
    unittest.TextTestRunner(verbosity=2).run(suite)
    p = Package()
    p.do_all()
